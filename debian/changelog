qlcplus (4.14.0-1) unstable; urgency=medium

  * New upstream version 4.14.0
  * d/control: Add libqt5websockets5-dev to Build-Depends
  * d/copyright: Remove entry of deleted file
  * d/lintian-overrides: Remove not needed overrides

 -- Jerome Lebleu <jerome@maroufle.fr>  Tue, 25 Feb 2025 19:05:30 +0100

qlcplus (4.13.1-1) unstable; urgency=medium

  [ Chris Hofstaedtler ]
  * Use udev.pc to place udev files (Closes: #1058833)

  [ Jerome Lebleu ]
  * New upstream version 4.13.1
  * Update Build-Depends, patches and dh_auto_configure for CMake switch
  * d/control:
    + Replace pkg-config by pkgconf in Build-Depends
    + Bump Standards-Versions to 4.7.0 (no changes)
  * d/copyright: Update years of active developers
  * d/lintian-overrides: Extend and update overrides for private libraries
  * d/rules: Do not add ldconfig trigger for private libraries

 -- Jerome Lebleu <jerome@maroufle.fr>  Sat, 01 Jun 2024 14:12:47 +0200

qlcplus (4.12.7-1) unstable; urgency=medium

  * New upstream version 4.12.7
  * d/control: Add python3 and python3-lxml to Build-Depends for fixtures-tool
  * d/control: Bump Standards-Versions to 4.6.2 (no changes)
  * d/patches: Use python3 interpreter for fixtures-tools

 -- Jerome Lebleu <jerome@maroufle.fr>  Thu, 25 May 2023 19:43:39 +0200

qlcplus (4.12.6-1) unstable; urgency=medium

  * New upstream version 4.12.6
  * d/patches: Remove upstream merged patches

 -- Jerome Lebleu <jerome@maroufle.fr>  Mon, 29 Aug 2022 15:25:10 +0200

qlcplus (4.12.5-3) unstable; urgency=medium

  * d/patches: Fix CID on loopback device in E1.31 plugin (Closes: #1015854)
  * d/upstream: Update the changelog with last releases

 -- Jerome Lebleu <jerome@maroufle.fr>  Sat, 23 Jul 2022 09:40:23 +0200

qlcplus (4.12.5-2) unstable; urgency=medium

  * d/patches: Fix rounded values in FadeChannel and KeyPadParser

 -- Jerome Lebleu <jerome@maroufle.fr>  Thu, 07 Jul 2022 14:54:54 +0200

qlcplus (4.12.5-1) unstable; urgency=medium

  * New upstream version 4.12.5
  * d/control: Bump Standards-Versions to 4.6.1 (no changes)
  * d/control: Update libusb dependency to libusb-1.0-0-dev
  * d/lintian-overrides: Update and fix mismatched overrides of RGB scripts
  * d/copyright: Update years for debian/*

 -- Jerome Lebleu <jerome@maroufle.fr>  Thu, 23 Jun 2022 12:23:35 +0200

qlcplus (4.12.4-1) unstable; urgency=medium

  * New upstream version 4.12.4
  * d/patches: Remove upstream merged patches
  * d/copyright: Update entry for licenseDMG.py
  * d/rules: Do not skip deprecated-declarations errors anymore

 -- Jerome Lebleu <jerome@maroufle.fr>  Tue, 24 Aug 2021 22:03:14 +0200

qlcplus (4.12.3-3) unstable; urgency=medium

  [ Jerome Lebleu ]
  * d/tests: Define tests which just check help output

  [ Debian Janitor ]
  * Apply multi-arch hints

 -- Jerome Lebleu <jerome@maroufle.fr>  Mon, 14 Jun 2021 15:25:22 +0200

qlcplus (4.12.3-2) unstable; urgency=medium

  * d/control:
    + Add libqt5multimedia5-plugins in Recommends for audio
      input/output and video function
    + Bump Standards-Versions to 4.5.1 (no changes)
  * d/patches: Fix some issues in AppStream MetaInfo
  * d/gbp.conf: Use debian/latest for the default packaging branch

 -- Jerome Lebleu <jerome@maroufle.fr>  Sun, 07 Mar 2021 12:36:48 +0100

qlcplus (4.12.3-1) unstable; urgency=medium

  * New upstream version 4.12.3
  * d/lintian: Add an override for
    package-contains-documentation-outside-usr-share-doc
  * d/patches: Remove now included patch
  * d/rules: Do not skip deprecated-copy errors anymore

 -- Jerome Lebleu <jerome@maroufle.fr>  Tue, 08 Sep 2020 15:57:17 +0200

qlcplus (4.12.2-4) unstable; urgency=medium

  * d/control: Remove libola-dev from Build-Depends (it has been removed from
    testing and seems currently unmaintained)
  * d/lintian:
    + Rename insane-line-length-in-source-file tag
    + Add an override for repackaged-source-not-advertised tag

 -- Jerome Lebleu <jerome@maroufle.fr>  Fri, 28 Aug 2020 22:23:13 +0200

qlcplus (4.12.2-3) unstable; urgency=medium

  * d/control:
    + Add libola-dev in Build-Depends
    + Specify Rules-Requires-Root: no
    + Bump Standards-Version to 4.5.0 (no changes needed)
    + Switch to debhelper-compat and level 13
    + Update Vcs-* URLs
  * d/patches: Compare fonts by strings and not by reference
  * d/rules:
    + Drop as-needed linker flag
    + Import default.mk instead of only buildflags.mk
    + Don't fail on deprecated-declarations errors (Closes: #964694)

 -- Jerome Lebleu <jerome@maroufle.fr>  Mon, 27 Jul 2020 11:01:05 +0200

qlcplus (4.12.2-2) unstable; urgency=medium

  * d/gbp.conf: Remove user-specific configuration
  * d/control:
    + Bump Standards-Version to 4.4.1 (no changes needed)
    + Switch upstream URL to HTTPS
  * d/watch: Ignore unstable releases by using special strings

 -- Jerome Lebleu <jerome@maroufle.fr>  Sun, 24 Nov 2019 15:51:20 +0100

qlcplus (4.12.2-1) unstable; urgency=medium

  * Initial release (Closes: #694077)

 -- Jerome Lebleu <jerome@maroufle.fr>  Mon, 26 Aug 2019 09:04:42 +0200
